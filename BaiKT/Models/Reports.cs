﻿namespace BaiKiemTra.Models
{
    public class Reports
    {
        public int ReportID { get; set; }
        public int AccountID { get; set; }
        public int LogsID { get; set; }
        public int TransactionalID { get; set; }
        public int Reportname { get; set; }
        public DateTime ReportDate { get; set; }
        public String add_text_here { get; set; }
    }
}
