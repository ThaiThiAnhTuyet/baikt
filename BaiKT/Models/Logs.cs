﻿namespace BaiKiemTra.Models
{
    public class Logs
    {
        public int LogID { get; set; }
        public int TransactionID { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }
        public String add_text_here { get; set; }
        public virtual Reports Reports { get; set; }
    }
}
